#include "ppconsul/kv.h"
#include "ppconsul/agent.h"

#include <semaphore.h>

enum class ServerResult: size_t
{
    Ok = 0,
    SocketError,
    ConsulAgentError,
    ConsulKeyValueError,
    SyncError,
    Unknown,
};
class CDataServer {
    public:
        CDataServer(ppconsul::Consul& m_consul, const std::string& serviceName, const uint16_t port);
        explicit CDataServer(ppconsul::Consul& m_consul, const uint16_t number);
        ~CDataServer();

        ServerResult Run();

    private:
        enum class ServerCommand: size_t {
            InputStr = 0,
            HealthCheck,
            Close,
            Unknown,
        };
        //Consul agent methods
        ServerResult Register() const;
        ServerResult Deregister() const;
        //Key-Value storage methods
        ServerResult SetKeyValue(const std::string& value) const;
        ServerResult Get10KeyValues(std::string& value) const;
        ServerResult EraseKeyValue() const;
        ServerCommand ParseCommand(const std::string& cmd);
        std::string ProcessInputString(const std::string& value) const;

        ServerResult m_state;
        ppconsul::Consul& m_consul;
        ppconsul::agent::Agent m_agent;
        ppconsul::kv::Kv m_keyValue;
        std::string m_serviceName;
        uint16_t m_port;
        sem_t* m_sem = nullptr;
        
};

void DataServer(uint8_t number, ppconsul::Consul& consul);