#include <iostream>
#include "ppconsul/agent.h"
#include "ppconsul/kv.h"
#include "service.h"
#include <stdio.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unordered_map>

const std::unordered_map<uint16_t, uint16_t> GetServices(const ppconsul::agent::Agent& agent) {
    auto services = agent.services();

    std::unordered_map<uint16_t, uint16_t> result;
    for (auto service: services) {
        if (service.second.name.find("consul-sample-") != std::string::npos) {
            unsigned int num;
            if (sscanf(service.second.name.c_str(), "consul-sample-%u", &num) == 1) {
                result.insert({num, service.second.port});
            }
        }
    }
    return result;
}

void StopAll(const ppconsul::agent::Agent& agent) {    
    auto& services = GetServices(agent);

    const std::string closeCommand("::close");

    for (auto& service: services) {
        struct sockaddr_in addr;
        int sock = socket(AF_INET, SOCK_STREAM, 0);

        addr.sin_family = AF_INET;
        addr.sin_port = htons(service.second);
        addr.sin_addr.s_addr = htonl(INADDR_LOOPBACK);
        connect(sock, (struct sockaddr *)&addr, sizeof(addr));

        send(sock, closeCommand.c_str(), closeCommand.size(), 0);
        close(sock);
    }
}

void SendString(const std::unordered_map<uint16_t, uint16_t>& services, const std::string& inp) {
    if (inp.find(":") == std::string::npos) {
        return;
    }
    char buf[1024];
    buf[0] = '\0';
    unsigned int num;
    if (sscanf(inp.c_str(),"%u:%s", &num, buf) < 2) {
        return;
    }
    auto it = services.find(num);
    if (it == services.end()) {
        return;
    }    

    auto port = it->second;
    struct sockaddr_in addr;
    int sock = socket(AF_INET, SOCK_STREAM, 0);

    addr.sin_family = AF_INET;
    addr.sin_port = htons(port);
    addr.sin_addr.s_addr = htonl(INADDR_LOOPBACK);
    connect(sock, (struct sockaddr *)&addr, sizeof(addr));

    send(sock, buf, strlen(buf), 0);
    size_t bytesRead {0U};    
    while ((bytesRead = recv(sock, buf, 1024, 0)) > 0)  {
        std::cout << std::string(buf, bytesRead);
    }
    std::cout << std::endl;
    close(sock);
}

int main(int argc, char **argv) {

    if (argc == 1) {
        std::cout << "Usage: consample { -c servers_count | -k | -i }" << std::endl;
        std::cout << "-s N: create N services and interaction it. Existing services will be closed" << std::endl;
        std::cout << "-k: close all existing sevices" << std::endl;
        std::cout << "-i: interaction with servicies. Send strings" << std::endl;
        std::cout << "e.g consample -c 5" << std::endl;
        std::cout << "e.g consample -k" << std::endl;
        std::cout << "e.g consample -i" << std::endl;
        return 0;
    }

    using ppconsul::Consul;
    using namespace ppconsul::agent;

    Consul consul;
    Agent agent(consul);

    size_t servicesCount {0U};

    const char *options = "s:ki";
    char opt;
    while((opt = getopt(argc, argv, options)) != -1) {
        switch(opt) {
            case 's':
                servicesCount = atoi(optarg);
                break;
            case 'k':
                StopAll(agent);
                return 0;
                break;
            case 'i': break;
        }
    }

    if (servicesCount > 0) {
        StopAll(agent);
    }

    while (servicesCount--) {
        DataServer(servicesCount, consul);
    }
    
    std::string inp;
    const std::string closeCommand("::close");
    for(;;) {
        auto services = GetServices(agent);
        std::cout << "Service numbers available:";
        for (auto service: services) {
            std::cout << service.first << " ";
        }
        std::cout << std::endl;

        std::cout << "Usage: type <service_number>:<string>. For example: \"0:sample string\". Type \"::close\" to quit" << std::endl;
        std::cout << ">";
        std::cin >> inp;
        if (inp.find(closeCommand) != std::string::npos) {
            break;
        }
        SendString(services, inp);
    }

    return 0;
}