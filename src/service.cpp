#include "service.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <netinet/in.h>

#include <string>
#include "ppconsul/agent.h"
#include "ppconsul/kv.h"

namespace {
    constexpr uint16_t startPort = 9876;
    const std::string closeCommand = "::close";
    const std::string semaphoreName = "ConsulSampleSemaphore";
    const std::string serviceNamePrefix = "consul-sample-";
    const std::string storagePrefix = ".storage";
    const std::string terminatedMessage = "Service Terminated";
}

CDataServer::CDataServer(ppconsul::Consul& consul, const std::string& serviceName, const uint16_t port)
: m_consul(consul)
, m_agent(consul)
, m_keyValue(consul)
, m_serviceName(serviceName)
, m_port(port)
, m_state(ServerResult::Ok) {

    m_sem = sem_open(semaphoreName.c_str(), O_CREAT, 0777, 1);
    if (SEM_FAILED != m_sem) {
        m_state = Register();
    } else {
        m_state = ServerResult::SyncError;
    }
}

CDataServer::CDataServer(ppconsul::Consul& consul, const uint16_t number)
: CDataServer(consul, serviceNamePrefix + std::to_string(number), startPort + number) {

}

ServerResult CDataServer::Register() const {
    ServerResult result = ServerResult::Ok;
    using namespace ppconsul::agent;
    sem_wait(m_sem);
    try {
        std::cout << "SEMAPHORE!!!" << std::endl;
        m_agent.registerService(
        kw::name = m_serviceName,
        kw::port = m_port,
        kw::tags = {"tcp"},
        kw::check = TcpCheck{"localhost:"+std::to_string(m_port), std::chrono::seconds(2)});
    } catch(...)
    {
        result = ServerResult::ConsulAgentError;
    }
    sem_post(m_sem);
    return result;
}

ServerResult CDataServer::Deregister() const {
    ServerResult result = ServerResult::Ok;
    sem_wait(m_sem);
    try {
        m_agent.deregisterService(m_serviceName);
    } catch(...)
    {
        result = ServerResult::ConsulAgentError;
    }
    sem_post(m_sem);
    return result;
}

ServerResult CDataServer::SetKeyValue(const std::string& value) const {
    ServerResult result = ServerResult::Ok;
    try {
        m_keyValue.set(storagePrefix+m_serviceName, value);
    } catch(...) {
        result = ServerResult::ConsulKeyValueError;
    }
    return result;
}

ServerResult CDataServer::Get10KeyValues(std::string& value) const {
    ServerResult result = ServerResult::Ok;
    value.clear();
    try {
        size_t count {10U};
        auto items = m_keyValue.items(storagePrefix);
            for (auto& item: items) {
                value.append(item.key + ":" + item.value + "\n\r");
                if (count-- == 0) break;
        }
    } catch(...) {
        result = ServerResult::ConsulKeyValueError;
        value.clear();
    }
    return result;
}

ServerResult CDataServer::EraseKeyValue() const {
    ServerResult result = ServerResult::Ok;
    try {
        m_keyValue.erase(storagePrefix+m_serviceName);
    }
    catch(...) {
        result = ServerResult::ConsulKeyValueError;
    }
    return result;
}

CDataServer::~CDataServer() {
    EraseKeyValue();
    Deregister();
}

CDataServer::ServerCommand CDataServer::ParseCommand(const std::string& cmd) {
        //Consul check health conection
        if (cmd.size() == 0) {
            return ServerCommand::HealthCheck;
        }

        //Close command            
        if (cmd.find(closeCommand) == 0) {
            return ServerCommand::Close;
        }

        return ServerCommand::InputStr;
}

std::string CDataServer::ProcessInputString(const std::string& value) const {
    ServerResult result = SetKeyValue(value);
    std::string output;
    if (ServerResult::Ok == result) {
        Get10KeyValues(output);
    } else {
        output = "ERROR: Key-Value Storage error!";
    }
    return output;
}

ServerResult CDataServer::Run() {
    if (ServerResult::Ok != m_state) {
        return m_state;
    }

    int listener;
    if ((listener = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        return m_state = ServerResult::SocketError;
    }

    struct sockaddr_in addr;
    addr.sin_family = AF_INET;
    addr.sin_port = htons(m_port);
    addr.sin_addr.s_addr = htonl(INADDR_ANY);
    if(bind(listener, (struct sockaddr *)&addr, sizeof(addr)) < 0) {
        return m_state = ServerResult::SocketError;
    }

    if ( listen(listener, 1) < 0) {
        return m_state = ServerResult::SocketError;
    }

    char buf[1024];
    int sock;
    bool terminated = false;
    while(!terminated)
    {
        if ((sock = accept(listener, NULL, NULL)) < 0) {
            return m_state = ServerResult::SocketError;
        }
        size_t bytesRead = recv(sock, buf, 1024, 0);
        std::string inp=std::string(buf, bytesRead);

        switch (ParseCommand(inp)) {            
            case ServerCommand::HealthCheck: /*nothing*/ break;
            case ServerCommand::Close: {
                send(sock, terminatedMessage.c_str(), terminatedMessage.size(), 0);
                terminated = true; 
            } break;
            case ServerCommand::InputStr: {                
                //Processing input string. Don't stop service if key-value failed. Send message to socket.
                std::string out = ProcessInputString(inp);
                send(sock, out.c_str(), out.size(), 0);
            } break;
            default: {/*unknown commands handling*/}
        }
        close(sock);
    }
    return m_state;
}

void DataServer(uint8_t number, ppconsul::Consul& consul) {
    int pid = fork();
    if (pid != 0) {
        //Parent process
        return;
    } else {
        //Child process. Service.

        //Demonize it
        close(STDOUT_FILENO);
        close(STDIN_FILENO);
        close(STDERR_FILENO);
        CDataServer dataServer(consul, number);
        dataServer.Run();
    }
    exit(0);
}